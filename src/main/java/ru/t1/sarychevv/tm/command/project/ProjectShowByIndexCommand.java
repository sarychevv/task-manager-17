package ru.t1.sarychevv.tm.command.project;

import ru.t1.sarychevv.tm.model.Project;
import ru.t1.sarychevv.tm.util.TerminalUtil;

public class ProjectShowByIndexCommand extends AbstractProjectCommand {

    @Override
    public String getDescription() {
        return "Show project by index.";
    }

    @Override
    public String getName() {
        return "project-show-by-index";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = getProjectService().findOneByIndex(index);
        showProject(project);
    }

}
