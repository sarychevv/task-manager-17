package ru.t1.sarychevv.tm.component;

import ru.t1.sarychevv.tm.api.repository.ICommandRepository;
import ru.t1.sarychevv.tm.api.repository.IProjectRepository;
import ru.t1.sarychevv.tm.api.repository.ITaskRepository;
import ru.t1.sarychevv.tm.api.service.*;
import ru.t1.sarychevv.tm.command.AbstractCommand;
import ru.t1.sarychevv.tm.command.project.*;
import ru.t1.sarychevv.tm.command.system.*;
import ru.t1.sarychevv.tm.command.task.*;
import ru.t1.sarychevv.tm.enumerated.Status;
import ru.t1.sarychevv.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.sarychevv.tm.exception.system.CommandNotSupportedException;
import ru.t1.sarychevv.tm.model.Project;
import ru.t1.sarychevv.tm.model.Task;
import ru.t1.sarychevv.tm.repository.CommandRepository;
import ru.t1.sarychevv.tm.repository.ProjectRepository;
import ru.t1.sarychevv.tm.repository.TaskRepository;
import ru.t1.sarychevv.tm.service.*;
import ru.t1.sarychevv.tm.util.TerminalUtil;

public final class Bootstrap implements IServiceLocator {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final ILoggerService loggerService = new LoggerService();

    {
        registry(new ApplicationAboutCommand());
        registry(new ApplicationExitCommand());
        registry(new ApplicationHelpCommand());
        registry(new ApplicationInfoCommand());
        registry(new ApplicationVersionCommand());
        registry(new ArgumentListCommand());
        registry(new CommandListCommand());

        registry(new ProjectListCommand());
        registry(new ProjectChangeStatusByIdCommand());
        registry(new ProjectChangeStatusByIndexCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectCompleteByIdCommand());
        registry(new ProjectCompleteByIndexCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectShowByIndexCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());

        registry(new TaskListCommand());
        registry(new TaskBindToProjectCommand());
        registry(new TaskChangeStatusByIdCommand());
        registry(new TaskChangeStatusByIndexCommand());
        registry(new TaskClearCommand());
        registry(new TaskCompleteByIdCommand());
        registry(new TaskCompleteByIndexCommand());
        registry(new TaskCreateCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskShowByProjectIdCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskStartByIndexCommand());
        registry(new TaskUnbindFromProjectCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateByIndexCommand());
    }

    private static void exit() {
        System.exit(0);
    }

    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @Override
    public ILoggerService getLoggerService() {
        return loggerService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    public void start(String[] args) {
        processArguments(args);

        initDemoData();
        initLogger();

        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND:");
                final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (final Exception e) {
                loggerService.error(e);
                System.out.println("[FAIL]");
            }
        }
        loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
    }

    private void initLogger() {
        loggerService.info("** WELCOME TO TASK-MANAGER **");
    }

    private void registry(final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    private void processArguments(final String[] args) {
        if (args == null || args.length == 0) return;
        processArgument(args[0]);
        System.exit(0);
    }

    private void processArgument(final String arg) {
        final AbstractCommand abstractCommand = commandService.getCommandByArgument(arg);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(arg);
        abstractCommand.execute();
    }

    private void processCommand(final String command) {
        final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        abstractCommand.execute();
    }

    private void initDemoData() {
        int n = (int) (1 + Math.random() * 20);
        for (int i = 0; i < n; i++) {
            final Project project = new Project();
            project.setName("TEST PROJECT " + i);
            project.setStatus(randStatus());
            projectService.add(project);
        }
        n = (int) (1 + Math.random() * 20);
        for (int i = 0; i < n; i++) {
            final Task task = new Task();
            task.setName("TEST TASK " + i);
            task.setStatus(randStatus());
            taskService.add(task);
        }
    }

    private Status randStatus() {
        int n = (int) (Math.random() * 2);
        switch (n) {
            case 0:
                return Status.IN_PROGRESS;
            case 1:
                return Status.COMPLETED;
            default:
                return Status.NOT_STARTED;
        }
    }
}
