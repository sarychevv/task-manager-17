package ru.t1.sarychevv.tm.exception.field;

public final class TaskEmptyException extends AbstractFieldException {

    public TaskEmptyException() {
        super("Error! Task is empty...");
    }

}
